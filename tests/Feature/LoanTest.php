<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoanTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    // test amount validation
    public function testAmountValidationOnLoanApply()
    {
        //Create user
        $user = User::create([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'password' => bcrypt('secret1234'),
        ]);

        $this->actingAs($user);

        //User's data
        $data = [
            'installment_no' => 6,
            'amount' => '',
        ];

        $response = $this->json('POST', '/api/loan/apply', $data);

        $response->assertStatus(422);
    }

    public function testInstallmentNoValidationOnLoanApply()
    {
        //Create user
        $user = User::create([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'password' => bcrypt('secret1234'),
        ]);

        $this->actingAs($user);

        //User's data
        $data = [
            'installment_no' => 6,
            'amount' => '',
        ];

        $response = $this->json('POST', '/api/loan/apply', $data);

        $response->assertStatus(422);
    }
}
