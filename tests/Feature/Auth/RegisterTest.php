<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    // Test Email Require Validation for User Register
    public function testRegisterEmailValidation()
    {
        //User's data
        $data = [
            'email' => '',
            'name' => 'Test',
            'password' => 'secret1234',
            'password_confirmation' => 'secret1234',
        ];

        $response = $this->json('POST', '/api/auth/register', $data);

        $response->assertStatus(422);
        $this->assertArrayHasKey('email', $response->json()['errors']);
    }

    // Test Name Require Validation for User Register
    public function testRegisterNameValidation()
    {
        //User's data
        $data = [
            'email' => 'test@gmail.com',
            'name' => '',
            'password' => 'secret1234',
            'password_confirmation' => 'secret1234',
        ];

        $response = $this->json('POST', '/api/auth/register', $data);

        $response->assertStatus(422);
        $this->assertArrayHasKey('name', $response->json()['errors']);
    }

    // Test Password Require Validation for User Register
    public function testRegisterPasswordValidation()
    {
        //User's data
        $data = [
            'email' => 'test@gmail.com',
            'name' => 'Test',
            'password' => '',
            'password_confirmation' => 'secret1234',
        ];

        $response = $this->json('POST', '/api/auth/register', $data);

        $response->assertStatus(422);
        $this->assertArrayHasKey('password', $response->json()['errors']);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRegiserUser()
    {
        //User's data
        $data = [
            'email' => 'test@gmail.com',
            'name' => 'Test',
            'password' => 'secret1234',
            'password_confirmation' => 'secret1234',
        ];

        $response = $this->json('POST', '/api/auth/register', $data);

        //Assert it was successful and a token was received
        $response->assertStatus(200);
        $this->assertArrayHasKey('token', $response->json()['data']);
    }

}
