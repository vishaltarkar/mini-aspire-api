<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    // Email Validation test for User Login
    public function testRegisterEmailValidation()
    {
        //User's data
        $data = [
            'email' => '',
            'password' => 'secret1234',
        ];

        $response = $this->json('POST', '/api/auth/login', $data);

        $response->assertStatus(422);
        $this->assertArrayHasKey('email', $response->json()['errors']);
    }

    // Password Validation test for User Login
    public function testRegisterPasswordValidation()
    {
        //User's data
        $data = [
            'email' => 'test@gmail.com',
            'password' => '',
        ];

        $response = $this->json('POST', '/api/auth/login', $data);

        $response->assertStatus(422);
        $this->assertArrayHasKey('password', $response->json()['errors']);
    }

    // Login failed test
    public function testLoginFail()
    {
        //Create user
        User::create([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'password' => bcrypt('secret1234'),
        ]);

        //attempt login with wrong password or email
        $response = $this->json('POST', '/api/auth/login', [
            'email' => 'test@gmail.com',
            'password' => 'secret12345',
        ]);

        //Assert it was successful and a token was received
        $response->assertStatus(401);
    }

    // Login success case
    public function testLoginSuccess()
    {
        // Create user
        User::create([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'password' => bcrypt('secret1234'),
        ]);

        // Attempt login
        $response = $this->json('POST', '/api/auth/login', [
            'email' => 'test@gmail.com',
            'password' => 'secret1234',
        ]);

        // Assert it was successful and a token was received
        $response->assertStatus(200);
        $this->assertArrayHasKey('token', $response->json()['data']);
    }
}
