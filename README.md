**Steps to installation**

1. create a `.env` file from `.env.example` & update the value according to your local setup.
2. run `composer install`
3. run `php artisan migrate` to create all the tables in database
4. run `php artisan db:seed` to create a user to access secure api OR you can use register API to create a new user.
5. Please find the postman collection file named `Postman/mini_aspire_api.json` & use them to access application.

**Project Brief**

It's a simple loan application app, where users registered and Logged In to the app and Can apply for Loans for a weekly tenure. Users also have access to see a list of their loans and their status with the next & last payment dates & can pay an installment of those loans.

**Brief for Developer/Technical Persons**

The Application use no extra package except for the `larvel sanctrum`. I tried to keep it very simple and it will work out of the box with just basic requirement.

Also, Requirment of the application doesnt have any special need which require any special package to be used.

*There are some points thought which I would like to look at.*

- Trait/ApiResponser.php - This trait is specifically create to make API response more generic and structure.
- You will find I have used `scope` instead of  writing `where` in controller. I find this method very clean and easy to read.

> You will also find basic unit testing for few end points.
