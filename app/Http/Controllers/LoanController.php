<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Loan;
use App\Models\LoanTransaction;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoanController extends Controller
{
    use ApiResponser;

    /**
     * Loan application will be submitted & get approved from here.
     *
     * @param Request $request
     * @param integer $installment_no
     * @param decimal $amount
     * @return json
     */
    public function applyLoan(Request $request)
    {
        // Validation
        $validated = $request->validate([
            'installment_no' => 'required|integer',
            'amount' => 'required|numeric',
        ]);

        $loggedInUser = $request->user(); // current user

        DB::beginTransaction();
        try {
            // Create a loan applied entry.
            $loan = Loan::create([
                'user_id' => $loggedInUser->id,
                'amount' => $request->amount,
                'interest_rate' => Loan::DEFAULT_INTEREST_RATE,
                'total_installment' => Loan::DEFAULT_INSTALLMENTS,
                'status' => Loan::STATUS_PROCESSING,
                'tenure_type' => Loan::TENURE_TYPE_WEEK,
                'applied_date' => Carbon::now(),
            ]);

            // approve loan ( just to justify that loan needs to be approved)
            $this->approveLoan($loan);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return $this->error("Loan application is failed. Please contact support.", $e->getCode());
        }

        return $this->success($this->getLoanList($request), 'Your loan has been approved.');
    }

    // an internal private function to automatically approve user loan.
    private function approveLoan($loan)
    {
        // update the status
        $loan->update([
            'status' => Loan::STATUS_APPROVED,
            'start_date' => Carbon::now(),
            'installment_amount' => $loan->emiCalculator(),
            'next_payment_date' => Carbon::now()->addWeek(),
        ]);
    }

    /**
     * Get a list of loan of users with status.
     *
     * @param string $status
     * @return json
     */
    public function getLoanList($status = null)
    {
        $results = Loan::with(['transactions'])->OfUserId($request->user()->id)
            ->select('id', 'amount', 'status', 'start_date', 'installment_amount', 'next_payment_date', 'last_payment_date');

        if ($status) {
            $results = $results->OfStatus($status);
        }

        $results = $results->get();
        return $this->success($results);
    }

    /**
     * To do a loan installment payment and change the status of the laon if its finished.
     *
     * @param Request $request
     * @param integer $request->loan_id
     * @return json
     */
    public function payInstallment(Request $request)
    {
        $validated = $request->validate([
            'loan_id' => 'required|integer|exists:loans,id',
        ]);

        // get loan details
        $loggedInUser = $request->user(); // current user
        $loan = Loan::find($request->loan_id);
        $loan_status = $loan->status;
        $allow_payment = false;

        // check if loan is already closed.
        if ($loan->status != Loan::STATUS_CLOSED) {

            // get previous payment details
            $loanTransactions = LoanTransaction::OfLoanId($request->loan_id)
                ->OfUserId($loggedInUser->id)->OfTransType(LoanTransaction::TRANS_TYPE_CREDIT)
                ->get();

            // if there is not transcation for a loan
            if (sizeof($loanTransactions) == 0) {
                $allow_payment = true;
            } elseif (sizeof($loanTransactions) < $loan->total_installment) {
                $allow_payment = true;
            } else {
                // if all installments have been paid closethe loan.
                $loan->update(['status' => Loan::STATUS_CLOSED, 'next_payment_date' => null]);
                return $this->error("Loan has already been closed.", 200);
            }
        } else {
            $loan->update(['next_payment_date' => null]);
            return $this->error("Loan has already been closed.", 200);
        }

        DB::beginTransaction();
        try {
            // if allow_payment flag is true only then allow user payment.
            if ($allow_payment) {
                // make a log of transaction of credited installment.
                $installment_payment = LoanTransaction::create([
                    'loan_id' => $loan->id,
                    'user_id' => $loggedInUser->id,
                    'trans_type' => LoanTransaction::TRANS_TYPE_CREDIT,
                    'trans_amount' => $loan->installment_amount,
                    'status' => LoanTransaction::STATUS_SUCCESS,
                ]);

                // on successfully installment payment update last_payment & next_payment date for user's loan & also status.
                if ($installment_payment) {

                    // check if all the installment has been paid or not. if yes change loan status to close
                    if (sizeof($loanTransactions) == $loan->total_installment) {
                        $loan_status = Loan::STATUS_CLOSED;
                        $next_payment_date = null;
                    } else {
                        if (sizeof($loanTransactions) != 0) {
                            $week = sizeof($loanTransactions) + 1;
                        } else {
                            $week = 1;
                        }
                        $next_payment_date = Carbon::parse($loan->start_date)->addWeeks($week);
                    }

                    $loan->update([
                        'last_payment_date' => Carbon::now(),
                        'next_payment_date' => $next_payment_date,
                        'status' => $loan_status,
                    ]);
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
            return $this->error("Installment payment is failed.", $e->getCode());
        }
        return $this->success([], 'Loan installment is credited successfully.');
    }
}
