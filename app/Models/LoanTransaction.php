<?php

namespace App\Models;

use App\Models\Loan;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanTransaction extends Model
{
    use HasFactory;

    const TRANS_TYPE_CREDIT = 'CR';
    const TRANS_TYPE_DEBIT = 'DR';

    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_SUCCESS = 'success';
    const STATUS_FAIL = 'fail';

    /**
     * The attributes that are mass assignable.
     *
     * @va0r array
     */
    protected $fillable = [
        'loan_id',
        'user_id',
        'trans_type',
        'trans_amount',
        'status',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function loan()
    {
        return $this->belongsTo(Loan::class, 'loan_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
     */
    public function scopeOfUserId(Builder $builder, int $user_id)
    {
        return $builder->where('user_id', $user_id);
    }

    public function scopeOfLoanId(Builder $builder, int $loan_id)
    {
        return $builder->where('loan_id', $loan_id);
    }

    public function scopeOfTransType(Builder $builder, string $type)
    {
        return $builder->where('trans_type', $type);
    }
}
