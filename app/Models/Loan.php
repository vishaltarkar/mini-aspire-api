<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    use HasFactory;

    const DEFAULT_INTEREST_RATE = 16; // default rate of interest annual
    const DEFAULT_INSTALLMENTS = 3; // default number of installment for loan

    // list of possible tenure of loan
    const TENURE_TYPE_WEEK = 'week';
    const TENURE_TYPE_MONTH = 'month';

    // list of status for loan
    const STATUS_DRAFT = 'draft';
    const STATUS_PROCESSING = 'processing ';
    const STATUS_REJECTED = 'rejected';
    const STATUS_APPROVED = 'approved';
    const STATUS_CLOSED = 'closed';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'amount',
        'interest_rate',
        'total_installment',
        'tenure_type',
        'status',
        'applied_date',
        'start_date',
        'installment_amount',
        'last_payment_date',
        'next_payment_date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'applied_date' => 'datetime',
        'start_date' => 'datetime',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function transactions()
    {
        return $this->hasMany(LoanTransaction::class, 'loan_id', 'id')->orderBy('created_at', 'DESC');
    }

    /*
    | ------------------------------------------------------------------------
    | FUNCTIONS
    | ------------------------------------------------------------------------
     */

    // Calculate EMI of the Loan
    public function emiCalculator()
    {
        $p = $this->attributes['amount'];
        $r = $this->attributes['interest_rate'];
        $t = $this->attributes['total_installment'];

        // one week interest
        $r = $r / (52 * 100);

        // one week period
        // $t = $t * 52;

        $emi = ($p * $r * pow(1 + $r, $t)) /
            (pow(1 + $r, $t) - 1);

        return $emi;
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
     */
    public function scopeOfUserId(Builder $builder, int $user_id)
    {
        return $builder->where('user_id', $user_id);
    }

    public function scopeOfStatus(Builder $builder, string $status)
    {
        return $builder->where('status', $status);
    }
}
