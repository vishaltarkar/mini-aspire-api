<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->decimal('amount', 10, 2);
            $table->decimal('interest_rate', 3, 2)->comment("In %");
            $table->integer('total_installment')->comment("No. of installment");
            $table->string('tenure_type', 50)->default('week')->comment("week, month");
            $table->string('status', 50)->default('draft')->comment('approved / rejected / processing / draft');
            $table->dateTime('applied_date')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('last_payment_date')->nullable();
            $table->dateTime('next_payment_date')->nullable();
            $table->decimal('installment_amount', 10, 2)->nullable();
            $table->timestamps();
        });

        Schema::table('loans', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loans', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('loans');
    }
}
