<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanTransactionLogsTable extends Migration
{
    //  id
    // - loan_id
    // - user_id
    // - trans_type (Cr/Dr)
    // - trans_amount
    // - trans_date
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('loan_id');
            $table->unsignedBigInteger('user_id');
            $table->string('trans_type', 5)->comment('CR & DR');
            $table->decimal('trans_amount', 10, 2);
            $table->string('status', 50)->comment("success, fail, in_progress");
            $table->timestamps();
        });

        Schema::table('loan_transactions', function (Blueprint $table) {
            $table->foreign('loan_id')->references('id')->on('loans')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_transactions', function (Blueprint $table) {
            $table->dropForeign(['loan_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('loan_transactions');
    }
}
