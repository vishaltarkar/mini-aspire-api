<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\LoanController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('/auth/register', [AuthController::class, 'register']); // for user register user

Route::post('/auth/login', [AuthController::class, 'login']); // for user login

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/me', function (Request $request) {
        return auth()->user();
    }); // to get current user

    // loan routes
    Route::prefix('loan')->group(function () {
        Route::post('apply', [LoanController::class, 'applyLoan']); // for user to apply loan
        Route::get('list/{?status}', [LoanController::class, 'getLoanList']); // to get list of user's loan
        Route::post('pay-installment', [LoanController::class, 'payInstallment']); //  pay the installment & update the loan status
    });

    Route::post('/auth/logout', [AuthController::class, 'logout']); // for user logout
});
